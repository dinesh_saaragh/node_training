
const express = require("express");
const app = express();
const mongoose = require('mongoose');
require("dotenv").config();
var device = require('express-device');

mongoose.connect(process.env.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});
app.use(device.capture());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    next();
  });
app.use(express.json())
const register = require('./API/router/register-router');

app.use(register)

// listen for requests
app.listen(3000, () => {

    console.log("Server is listening on port 3000");
});