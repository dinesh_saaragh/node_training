const register = require('../model/register.model')
const device = require('../model/device-log.model');
const requestIp = require('request-ip');
const Bcrypt = require("bcryptjs");
var crypto = require('crypto');
const { sign } = require("jsonwebtoken");
require("dotenv").config();
const axios = require('axios');
const moment = require('moment');
module.exports = {
  createRegister: (req, res) => {
    console.log(req);
    try {
      req.body.password = Bcrypt.hashSync(req.body.password, 10);
      let { fname, lname, email, phone, address, password, active, active_code, date } = req.body;
      active = "false";
      date = new Date()

      crypto.randomBytes(10, async function (err, buf) {

        // Ensure the activation code is unique.
        active_code = buf.toString('hex');
        console.log("^^", active_code);
        let userRegister = new register({ fname, lname, email, phone, address, password, active, active_code, date });

        let ret = await userRegister.save();
        var link = await 'http://locolhost:3000/account/active?' + active_code;
        console.log("link", link);
        res.json({
          message: 'success',
          data: { 'email': ret.email }
        });
        /**
         * call email node server
         */
        sentEmail(email, link)
      });



    } catch (error) {
      res.json({
        message: 'failure',
        data: error
      });
    }
  },
  /**
   *  Check active code 
   *  validation
   */
  activeCodeValidation: async (req, res) => {
    console.log(req.body.date);
    let result, checkTime;

    try {
      checkTime = await register.find({ 'active_code': req.body.active_code })
      console.log("server", checkTime[0].date);
      console.log("user", req.body.date);
      let serverTime = moment(new Date(checkTime[0].date)).format('HH:mm:ss');
      let userTime = moment(new Date(req.body.date)).format('HH:mm:ss');
      let totalDate = parseInt(userTime) - parseInt(serverTime);
      if (totalDate < 1) {
        console.log("not expired");
        result = await register.findOneAndUpdate({ 'active_code': req.body.active_code },
        {

          $set: {
            active: 'true'
          }
        }
      )
      console.log("result",result);
      if (result != null) {
        res.json({
          code: 200,
          message: 'Email verified successfully',
        });
      } else {
        res.json({
          code: 404,
          message: 'Invalid User...',

        });
      }
        }
      else { 
        console.log("expired");
        res.json({
          message: 'Time Expired',

        });
       }
    
    } catch (error) {
      res.json({
        message: 'failure',
        data: error
      });
    }
  },
  /**
   * Resent mail 
   */
  resentMail: async (req, res) => {
    console.log(req.body.email);
    let result;
    try {
      crypto.randomBytes(10, async function (err, buf) {
        // Ensure the activation code is unique.
        active_code = buf.toString('hex');
        console.log("569", active_code);
        result = await register.findOneAndUpdate({ 'email': req.body.email },
          {
            $set: {
              active: 'false',
              date: new Date,
              active_code: active_code
            }
          }
        )
        var link = await 'http://locolhost:3000/account/active?' + active_code;
        console.log("link", link);
        if (result != null) {
          res.json({
            code: 200,
            message: 'success',
            data: result
          });
          sentEmail(req.body.email, link);
        } else {
          res.json({
            code: 404,
            message: 'Invalid User...',

          });
        }

      });
    } catch (error) {
      res.json({
        message: 'failure',
        data: error
      });
    }
  },
  login: async (req, res) => {
    try {
      let ip = requestIp.getClientIp(request);
      let deviceType = req.device.type;
      let otherOption = req.device.parser;
      let userLog = new device({ ip, deviceType, otherOption });
      let store = await userLog.save();

      let { email, userid } = req.body;
      let ret = await register.find({ $and: [{ 'email': email, '_id': userid }] });
      let jsontoken = sign({ ret: ret }, process.env.ACCESS_TOKEN, {
        expiresIn: "1h"
      });
      res.json({
        message: 'success',
        data: ret,
        token: jsontoken
      });
    } catch (error) {
      res.json({
        message: 'failure',
        data: error
      });
    }
  },

  getAllUser: async (req, res) => {
    try {
      let ret = await register.find({});
      res.json({
        message: 'success',
        data: ret
      });
    } catch (error) {
      res.json({
        message: 'failure',
        data: error
      });
    }

  }
}
function sentEmail(email, link) {
  console.log('fop',email, link);
  axios.post('http://localhost:4000/send-email', {
    email: email,
    message: link
  })
    .then(function (response) {
      res.json({
        message: 'success',
        data: response
      });

    })
    .catch(function (error) {
      console.log(error);
    });
}
