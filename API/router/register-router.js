const router = require("express").Router();
const {createRegister,login,getAllUser,activeCodeValidation,resentMail}=require('../controller/register.controller');
const {validateToken}=require('../../token/jwt')
router.post('/register',createRegister);
router.post('/login',login);
router.post('/verification/token',activeCodeValidation);
router.post('/verification/resent',resentMail);
router.get('/register/all',validateToken,getAllUser);
module.exports = router;