const mongoose=require('mongoose');

    let deviceSchema=mongoose.Schema({
        ip:{
            type:String,
            required: [true, "can't be empty"]
        },
        deviceType:{
            type:String,
            required: [true, "can't be empty"]
        },
        otherOption:{
            type:String,
            required: [true, "can't be empty"]
        }

    })
  

    module.exports=mongoose.model('devicelog',deviceSchema,'devicelog')