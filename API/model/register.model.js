const mongoose=require('mongoose');

    let registerSchema=mongoose.Schema({
        fname:{
            type:String,
            required: [true, "can't be empty"]
        },
        lname:{
            type:String,
            required: [true, "can't be empty"]
        },
        email:{
            type:String,
            unique: true,
            required: [true, "can't be empty"]
        },
        phone:{
            type:Number,
            required: [true, "can't be empty"]
        },
        password:{
            type:String,
            required: [true, "can't be empty"]
        },
        address:{
            type:String,
            required: [true, "can't be empty"]
        },
        active:{
            type:String,
        },
        active_code:{
            type:String,
            unique: true,
        },
        date:{ type:String}
    },{
        timeStamps:true
    })
  

    module.exports=mongoose.model('register',registerSchema,'register')